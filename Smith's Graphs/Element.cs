﻿namespace Smith_s_Graphs
{
    public class Element
    {
        public int Quantity { get; set; }

        public string Label { get; set; }
    }
}