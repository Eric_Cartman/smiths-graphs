﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smith_s_Graphs
{
    public class Node
    {
        public List<Element> CurrentGraph { get; set; }

        public List<int> TransformationList { get; set; }

        public List<Element> ParentGraph { get; set; }
    }
}
