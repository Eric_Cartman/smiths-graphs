﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Smith_s_Graphs
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.Write(@"   ______                                 __                __ _  __                                       __         
  / ____/____   _____ ____   ___   _____ / /_ _____ ____ _ / /(_)/ /_ __  __   ____ _ _____ ____ _ ____   / /_   _____
 / /    / __ \ / ___// __ \ / _ \ / ___// __// ___// __ `// // // __// / / /  / __ `// ___// __ `// __ \ / __ \ / ___/
/ /___ / /_/ /(__  )/ /_/ //  __// /__ / /_ / /   / /_/ // // // /_ / /_/ /  / /_/ // /   / /_/ // /_/ // / / /(__  ) 
\____/ \____//____// .___/ \___/ \___/ \__//_/    \__,_//_//_/ \__/ \__, /   \__, //_/    \__,_// .___//_/ /_//____/  
                  /_/                                              /____/   /____/             /_/                    
Cospectrality graphs of Smith graphs v1.0");
            Console.WriteLine();
            Execute();
        }

        private static void Execute()
        {
            Console.Write("\nUnesite Smitov graf u kanonskoj reprezentaciji.\nPuteve i konture unesite u istom redu i odvojite ih plusevima.\nPrimer ulaza: P2 + 2*C4\n");

            string input = Console.ReadLine();

            var inputGraph = TransformInput(input);

            if(inputGraph.Count == 0)
            {
                Console.WriteLine("Ulaz nije validan!\n");
            }
            else
            {

                int n = CalculateN(inputGraph);

                if(n == 0)
                {
                    Console.WriteLine("Ulaz nije validan!\n");
                }
                else
                {
                    var transformations = GetTransformations(n);

                    var uniqueNodes = new List<Node>();

                    var nodes = new List<Node>();

                    nodes.Add(new Node
                    {
                        CurrentGraph = inputGraph,
                        TransformationList = new List<int>(),
                        ParentGraph = null
                    });

                    uniqueNodes.Add(new Node
                    {
                        CurrentGraph = inputGraph,
                        TransformationList = new List<int>(),
                        ParentGraph = null
                    });

                    var i = 0;
                    var step = 1;
                    var currentGraph = nodes.FirstOrDefault();
                    while (i < transformations.Count)
                    {
                        bool canTransform = CanITransform(currentGraph.CurrentGraph, transformations[i]);
                        if (canTransform && !nodes.Where(x => x.CurrentGraph.SequenceEqual(currentGraph.CurrentGraph)).FirstOrDefault().TransformationList.Contains(i + 1))
                        {
                            var currentDisplay = "";
                            for (int l = 0; l < currentGraph.CurrentGraph.Count; l++)
                            {
                                if (l == currentGraph.CurrentGraph.Count - 1)
                                {
                                    if (currentGraph.CurrentGraph[l].Quantity > 1)
                                    {
                                        currentDisplay += currentGraph.CurrentGraph[l].Quantity.ToString() + "*";
                                    }
                                    currentDisplay += currentGraph.CurrentGraph[l].Label;
                                }
                                else
                                {
                                    if (currentGraph.CurrentGraph[l].Quantity > 1)
                                    {
                                        currentDisplay += currentGraph.CurrentGraph[l].Quantity.ToString() + "*";
                                    }
                                    currentDisplay += currentGraph.CurrentGraph[l].Label + " + ";
                                }
                            }
                            Console.WriteLine("====================================================================================================");
                            Console.WriteLine("Trenutni graf: " + currentDisplay);
                            Console.WriteLine("Izvršena transformacija br. " + (i + 1).ToString() + ": " + transformations[i].ToString());
                            nodes.Where(x => x.CurrentGraph.SequenceEqual(currentGraph.CurrentGraph)).FirstOrDefault().TransformationList.Add(i + 1);
                            var newNode = new Node
                            {
                                ParentGraph = currentGraph.CurrentGraph.ToList(),
                                CurrentGraph = Transform(currentGraph.CurrentGraph, transformations[i]),
                                TransformationList = new List<int>()
                            };
                            nodes.Add(newNode);
                            if (IsUnique(uniqueNodes, newNode))
                            {
                                uniqueNodes.Add(newNode);
                            }
                            i = 0;
                            currentGraph = new Node
                            {
                                ParentGraph = newNode.ParentGraph.ToList(),
                                CurrentGraph = newNode.CurrentGraph.ToList(),
                                TransformationList = newNode.TransformationList.ToList()
                            };
                            var solution = "";
                            for (int l = 0; l < currentGraph.CurrentGraph.Count; l++)
                            {
                                if (l == currentGraph.CurrentGraph.Count - 1)
                                {
                                    if (currentGraph.CurrentGraph[l].Quantity > 1)
                                    {
                                        solution += currentGraph.CurrentGraph[l].Quantity.ToString() + "*";
                                    }
                                    solution += currentGraph.CurrentGraph[l].Label;
                                }
                                else
                                {
                                    if (currentGraph.CurrentGraph[l].Quantity > 1)
                                    {
                                        solution += currentGraph.CurrentGraph[l].Quantity.ToString() + "*";
                                    }
                                    solution += currentGraph.CurrentGraph[l].Label + " + ";
                                }
                            }
                            Console.WriteLine("Rešenje br. " + step.ToString() + ": " + solution);
                            step++;
                        }
                        else
                        {
                            i++;
                        }
                        if (i == transformations.Count)
                        {
                            if (currentGraph.ParentGraph == null)
                            {
                                Console.WriteLine("====================================================================================================");
                                Console.WriteLine("Sve moguće transformacije su izvršene.");
                                var unique = nodes.GroupBy(x => x.CurrentGraph).Distinct();

                                break;
                            }
                            else
                            {
                                currentGraph = nodes.Where(x => x.CurrentGraph.SequenceEqual(currentGraph.ParentGraph)).FirstOrDefault();
                                i = 0;
                            }
                        }
                    }
                    Console.WriteLine("====================================================================================================");
                    Console.WriteLine("Sva rešenja:");
                    for (int counter = 0; counter < uniqueNodes.Count; counter++)
                    {
                        var currentDisplay = "";
                        for (int l = 0; l < uniqueNodes[counter].CurrentGraph.Count; l++)
                        {
                            if (l == uniqueNodes[counter].CurrentGraph.Count - 1)
                            {
                                if (uniqueNodes[counter].CurrentGraph[l].Quantity > 1)
                                {
                                    currentDisplay += uniqueNodes[counter].CurrentGraph[l].Quantity.ToString() + "*";
                                }
                                currentDisplay += uniqueNodes[counter].CurrentGraph[l].Label;
                            }
                            else
                            {
                                if (uniqueNodes[counter].CurrentGraph[l].Quantity > 1)
                                {
                                    currentDisplay += uniqueNodes[counter].CurrentGraph[l].Quantity.ToString() + "*";
                                }
                                currentDisplay += uniqueNodes[counter].CurrentGraph[l].Label + " + ";
                            }
                        }
                        Console.WriteLine((counter + 1).ToString() + ". " + currentDisplay);
                    }
                }
            }
                
            Console.WriteLine("Pritisnite bilo koji taster da resetujete program...");
            Console.ReadKey();
            Execute();
        }

        private static List<Element> Transform(List<Element> currentGraph, Transformation transformation)
        {
            List<Element> temp = new List<Element>();
            foreach(var el in currentGraph)
            {
                temp.Add(new Element
                {
                    Label = el.Label,
                    Quantity = el.Quantity
                });
            }
            int i = 0;
            while(i < temp.Count)
            {
                bool removed = false;
                Element el = temp[i];
                if (transformation.RightSide.FirstOrDefault(x => x.Label == el.Label && x.Quantity == el.Quantity) != null)
                {
                    temp.Remove(el);
                    removed = true;
                }
                else 
                {
                    var element = transformation.RightSide.FirstOrDefault(x => x.Label == el.Label);
                    if (element != null && (element.Quantity < el.Quantity))
                    {
                        el.Quantity = el.Quantity - element.Quantity;
                    }
                }
                if(!removed)
                {
                    i++;
                }
            }
            foreach(var ls in transformation.LeftSide)
            {
                if(temp.Where(x => x.Label == ls.Label).FirstOrDefault() != null)
                {
                    temp.Where(x => x.Label == ls.Label).FirstOrDefault().Quantity += ls.Quantity;
                }
                else
                {
                    temp.Add(new Element
                    {
                        Label = ls.Label,
                        Quantity = ls.Quantity
                    });
                }
            }
            return temp;
        }

        private static bool CanITransform(List<Element> currentGraph, Transformation transformation)
        {
            bool canTransform = true;
            foreach(Element el in transformation.RightSide)
            {
                var element = currentGraph.FirstOrDefault(x => x.Label == el.Label);

                bool test2 = element != null && (element.Quantity < el.Quantity);

                if (currentGraph.FirstOrDefault(x => x.Label == el.Label && x.Quantity == el.Quantity) == null)
                {
                    if(element == null || (element != null && element.Quantity < el.Quantity))
                    {
                        canTransform = false;
                        break;
                    }
                }
            }
            return canTransform;
        }

        private static List<Element> TransformInput(string input)
        {
            var toReturn = new List<Element>();
            try
            {
                var inputs = input.ToUpper().Replace(" ", String.Empty).Split('+');

                foreach (string s in inputs)
                {
                    if(s != "")
                    {
                        if (s.Contains("*"))
                        {
                            toReturn.Add(
                                new Element
                                {
                                    Label = s.Substring(s.IndexOf("*") + 1, s.Length - 1 - s.IndexOf("*")),
                                    Quantity = int.Parse(s.Substring(0, s.IndexOf("*")))
                                });
                        }
                        else
                        {
                            toReturn.Add(
                                new Element
                                {
                                    Label = s,
                                    Quantity = 1
                                });
                        }
                    }
                }
            }
            catch
            {
                
            }
            return toReturn;
        }

        private static int CalculateN(List<Element> inputGraph)
        {
            var n = 0;
            try
            {
                foreach (Element e in inputGraph)
                {
                    var l = int.Parse(e.Label.Substring(1));
                    n = n + e.Quantity * l;
                }
            }
            catch
            {
                n = 0;
            }
            return n;
        }

        private static bool IsUnique(List<Node> n1, Node n2)
        {
            bool isUnique = true;
            foreach(Node n in n1)
            {
                int count = 0;
                if (n.CurrentGraph.Count == n2.CurrentGraph.Count)
                {
                    for (int i = 0; i < n2.CurrentGraph.Count; i++)
                    {
                        for(int j = 0; j < n2.CurrentGraph.Count; j++)
                        {
                            if(n.CurrentGraph[i].Label == n2.CurrentGraph[j].Label && n.CurrentGraph[i].Quantity == n2.CurrentGraph[j].Quantity)
                            {
                                count++;
                            }
                        }
                    }
                }
                if (count == n2.CurrentGraph.Count)
                {
                    isUnique = false;
                    break;
                }
            }
            return isUnique;
        }

        private static List<Transformation> GetTransformations(int n)
        {
            var toReturn = new List<Transformation>();
            for(int i = 0; i < n; i++)
            {
                toReturn.Add(new Transformation //"Wn = C4 + Pn"
                {
                    LeftSide = new List<Element>() 
                    {
                        new Element
                        {
                            Label = "W" + (i + 1).ToString(),
                            Quantity = 1
                        }
                    },
                    RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "C4",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P" + (i + 1).ToString(),
                            Quantity = 1
                        }
                    }
                });
                if(i > 1)
                {
                    toReturn.Add(new Transformation //"Zn + Pn = P2n+1 + P1"
                    {
                        LeftSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "Z" + i.ToString(),
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P" + i.ToString(),
                            Quantity = 1
                        }
                    },
                        RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "P" + (2*i + 1).ToString(),
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P1",
                            Quantity = 1
                        }
                    }
                    });
                }
                if(i > 1)
                {
                    toReturn.Add(new Transformation //"C2n + 2P1 = C4 + 2Pn-1"
                    {
                        LeftSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "C" + (2*(i + 1)).ToString(),
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P1",
                            Quantity = 2
                        }
                    },
                        RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "C4",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P" + ((i + 1) - 1).ToString(),
                            Quantity = 2
                        }
                    }
                    });
                }
            }
            var staticTrans = new List<Transformation>() {
                new Transformation  //T1 + P5 + P3 = P11 + P2 + P1
                {
                    LeftSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "T1",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P5",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P3",
                            Quantity = 1
                        }
                    },
                    RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "P11",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P2",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P1",
                            Quantity = 1
                        }
                    }
                },
                new Transformation  //T2 + P8 + P5 = P17 + P2 + P1
                {
                    LeftSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "T2",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P8",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P5",
                            Quantity = 1
                        }
                    },
                    RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "P17",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P2",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P1",
                            Quantity = 1
                        }
                    }
                },
                new Transformation  //T3 + P14 + P9 + P5 = P29 + P4 + P2 + P1
                {
                    LeftSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "T3",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P14",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P9",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P5",
                            Quantity = 1
                        }
                    },
                    RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "P29",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P4",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P2",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P1",
                            Quantity = 1
                        }
                    }
                },
                new Transformation  //T4 + P1 = C4 + 2P2
                {
                    LeftSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "T4",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P1",
                            Quantity = 1
                        }
                    },
                    RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "C4",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P2",
                            Quantity = 2
                        }
                    }
                },
                new Transformation  //T5 + P1 = C4 + P3 + P2
                {
                    LeftSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "T5",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P1",
                            Quantity = 1
                        }
                    },
                    RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "C4",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P3",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P2",
                            Quantity = 1
                        }
                    }
                },
                new Transformation  //T6 + P1 = C4 + P4 + P2
                {
                    LeftSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "T6",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P1",
                            Quantity = 1
                        }
                    },
                    RightSide = new List<Element>()
                    {
                        new Element
                        {
                            Label = "C4",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P4",
                            Quantity = 1
                        },
                        new Element
                        {
                            Label = "P2",
                            Quantity = 1
                        }
                    }
                },

        };
            toReturn.AddRange(staticTrans);
            return toReturn;
        }
    }
}

            //"Wn = C4 + Pn",
            //"Zn + Pn = P2n+1 + P1",
            //"C2n + 2P1 = C4 + 2Pn-1",
            //"T1 + P5 + P3 = P11 + P2 + P1",
            //"T2 + P8 + P5 = P17 + P2 + P1",
            //"T3 + P14 + P9 + P5 = P29 + P4 + P2 + P1",
            //"T4 + P1 = C4 + 2P2",
            //"T5 + P1 = C4 + P3 + P2",
            //"T6 + P1 = C4 + P4 + P2"
