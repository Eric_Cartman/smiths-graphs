﻿using System.Collections.Generic;

namespace Smith_s_Graphs
{
    public class Transformation
    {
        public List<Element> LeftSide { get; set; }

        public List<Element> RightSide { get; set; }

        public override string ToString()
        {
            string s = "";
            for(int i = 0; i < LeftSide.Count; i++)
            {
                if(LeftSide[i].Quantity > 1)
                {
                    s += LeftSide[i].Quantity.ToString() + "*";
                }
                if(i == LeftSide.Count - 1)
                {
                    s += LeftSide[i].Label + " = ";
                }
                else
                {
                    s += LeftSide[i].Label + " + ";
                }
            }
            for (int i = 0; i < RightSide.Count; i++)
            {
                if (RightSide[i].Quantity > 1)
                {
                    s += RightSide[i].Quantity.ToString() + "*";
                }
                if (i == RightSide.Count - 1)
                {
                    s += RightSide[i].Label;
                }
                else
                {
                    s += RightSide[i].Label + " + ";
                }
            }
            return s;
        }
    }
}
